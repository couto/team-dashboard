## Development

## Server

To run the server in development mode:

```
yarn start:dev
```

### GoCD

To start a Docker container running a GoCD instance run the following command:

```
docker run --name server -it -p8153:8153 -p8154:8154 -d -v "$(PWD)/.data/storage":/godata -v "$(PWD)/.data/home:/home/go" gocd/gocd-server:v17.8.0
```


